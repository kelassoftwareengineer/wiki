# Problem Statement

| Story                                                                             | Estimation | Taken at week |
|-----------------------------------------------------------------------------------|------------|---------------|
| As a user I can add tasks to the list                                             | 3 | 2 |
| As a user I can see all the tasks in a list                                       | 3 | 2 |
| As a user I can delete a task                                                     | 3 | 3 |
| As a user I can update a task                                                     | 3 | 3 |
| As a user I can see uncompleted tasks and completed tasks list in different menus | - | - |
| As a user I can set category to tasks                                             | - | - |
| As a user I can see tasks list per category                                       | - | - |

Note:
Estimation points are 1 - 3. 1 means 1 hour working duration, and 3 means 3 hour working duration.

## Examples

These screenshots are project made with ruby, please adjust to it.

![example 1](../image/homework-problem-statement-example-img-1.png "example 1")

![example 2](../image/homework-problem-statement-example-img-2.png "example 2")

![example 3](../image/homework-problem-statement-example-img-3.png "example 3")
