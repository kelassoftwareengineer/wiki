# Homework Week 2

1. Make a group that consist 2-3 people. We prefer that you make a group with people you don't know. Don't be alone!
2. [*Class Homework*] Please contribute to this Wiki by creating new page about the group assignment (point 1) under `/homework`. Please follow [contribution guideline](../CONTRIBUTING.md) and open merge request before monday, 25 November 2019 22.00. 
3. Read [README.md](https://gitlab.com/kelassoftwareengineer/kelassoftwareengineer.gitlab.io/blob/master/README.md) of this repository.
4. Create a java project and work on the stories in [problem statement](problem-statement.md). After finished, please create a new repository in [week 2](https://gitlab.com/kelassoftwareengineer/week2) with format [alias-1_alias-2-alias-n]-todolistapp (example: qornanali_saifulwebid-todolistapp). Use the same contribution process with [contribution guideline](../CONTRIBUTING.md). Then, push the project to that repository before thursday, 28 November 2019 21.00 so that mentors can review your merge request.
5. Read blog ["Introduction to Clean Code"](https://hackernoon.com/how-to-write-clean-code-d557d998bb08) by Hackernoon.
6. Write lesson learned that you got in Week 2. Please follow this [guide](../tutorial/writing-lesson-learned.md). (*Not mandatory*)
