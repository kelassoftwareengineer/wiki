# Writing Lesson Learned

1. Clone this repository.

2. Create new markdown file under `./blog/<your username>/`. Give filename that url friendly and use lowercase characters. Example: `qornanali-tdd-is-test-driven-development.md`.

3. Write down your awesome and insightful experience. ![step 1](../image/cat-typing.gif)

4. Reference it on [SUMMARY.md](`../SUMMARY.md`).

5. Git commit it and push to branch master.

6. Your lesson learned have been published! 🎉
